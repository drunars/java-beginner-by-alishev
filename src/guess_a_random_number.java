
//V_1.6 fix bag min!=max

import java.util.Scanner;
//задача, сделать программу, которая будет загадывать рандомно одну цифру от мин.заданного значения до макс., и человек должен ее угадать.
//ввод с клавиатуры, + счетчик попыток.
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Игра: 'Угадай Число'" );
        System.out.println("Введите число минимального числа, от которого нужно угадывать");
        int min = scanner.nextInt();
        System.out.println("Введите число максимального числа, от которого нужно угадывать");
        int max = scanner.nextInt();

        if(min==max){
            System.out.println("Диапазон таким не может быть! Загадывай по новой :(");
        } else if(min!=max){
            System.out.println("Угадайте число от " + min + " до " + max + " (^_^)");
            System.out.println("Введите ваш вариант:");

            int value = scanner.nextInt(); //ввод с клавиатуры
            if(value>max){
                System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
            } else if(value<min) {
                System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
            }
            int ran = Random(min,max);
            int counter = 1;

            while(value!=ran){

                if(value!=ran){
                    counter++;
                }
                System.out.println("Не угадали, попробуйте еще раз :)");
                value = scanner.nextInt();
                if(value>max){
                    System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
                } else if(value<min) {
                    System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
                }

            }
            System.out.println("Поздравляю, вы угадали!");
            System.out.println("Верное число было: " + ran);
            System.out.println("Колличество попыток: " + counter );
        }




    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}





//====================================================
//V_1.5
/*
import java.util.Scanner;
//задача, сделать программу, которая будет загадывать рандомно одну цифру от 0 до 10, и человек должен ее угадать.
//ввод с клавиатуры. Затем во второй версии сделать счетчик, со скольки попыток человек угадал.
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Игра: 'Угадай Число'" );
        System.out.println("Введите число минимального числа, от которого нужно угадывать");
        int min = scanner.nextInt();
        System.out.println("Введите число максимального числа, от которого нужно угадывать");
        int max = scanner.nextInt();

        System.out.println("Угадайте число от " + min + " до " + max + " (^_^)");
        System.out.println("Введите ваш вариант:");

        int value = scanner.nextInt(); //ввод с клавиатуры
        if(value>max){
            System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
        } else if(value<min) {
            System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
        }
        int ran = Random(min,max);
        int counter = 1;

        while(value!=ran){

            if(value!=ran){
                counter++;
            }
            System.out.println("Не угадали, попробуйте еще раз :)");
            value = scanner.nextInt();
            if(value>max){
                System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
            } else if(value<min) {
                System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
            }

        }
        System.out.println("Поздравляю, вы угадали!");
        System.out.println("Верное число было: " + ran);
        System.out.println("Колличество попыток: " + counter );


    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}
*/
//==============================================================
//V_1.4
/*
import java.util.Scanner;
//задача, сделать программу, которая будет загадывать рандомно одну цифру от 0 до 10, и человек должен ее угадать.
//ввод с клавиатуры. Затем во второй версии сделать счетчик, со скольки попыток человек угадал.
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Игра: 'Угадай Число'" );
        System.out.println("Введите число минимального числа, от которого нужно угадывать");
        int min = scanner.nextInt();
        System.out.println("Введите число максимального числа, от которого нужно угадывать");
        int max = scanner.nextInt();


  //      int min = 0;
  //      int max = 20;


        System.out.println("Угадайте число от " + min + " до " + max + " (^_^)");
        System.out.println("Введите ваш вариант:");

        int value = scanner.nextInt(); //ввод с клавиатуры
        if(value>min){
            System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
        } else if(value<min) {
            System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
        }
        int ran = Random(min,max);
        int counter = 1;

        while(value!=ran){

            if(value!=ran){
                counter++;
            }
            System.out.println("Не угадали, попробуйте еще раз :)");
            value = scanner.nextInt();
            if(value>max){
                System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
            } else if(value<min) {
                System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
            }

        }
        System.out.println("Поздравляю, вы угадали!");
        System.out.println("Верное число было: " + ran);
        System.out.println("Колличество попыток: " + counter );


    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}

 */

//==============================================================
//V1_3.1
/*
import java.util.Scanner;
//задача, сделать программу, которая будет загадывать рандомно одну цифру от 0 до 10, и человек должен ее угадать.
//ввод с клавиатуры. Затем во второй версии сделать счетчик, со скольки попыток человек угадал.
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        int min = 0;
        int max = 20;
        System.out.println("Угадайте число от " + min + " до " + max + " (^_^)");
        System.out.println("Введите ваш вариант:");
        int value = scanner.nextInt(); //ввод с клавиатуры
        if(value>min){
            System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
        } else if(value<min) {
            System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
        }
        int ran = Random(min,max);
        int counter = 1;

        while(value!=ran){

            if(value!=ran){
                counter++;
            }
            System.out.println("Не угадали, попробуйте еще раз :)");
            value = scanner.nextInt();
            if(value>max){
                System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
            } else if(value<min) {
                System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
            }

        }
        System.out.println("Поздравляю, вы угадали!");
        System.out.println("Верное число было: " + ran);
        System.out.println("Колличество попыток: " + counter );


    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}
 */
//======================================================================
//V_1.3
/*
import java.util.Scanner;
//задача, сделать программу, которая будет загадывать рандомно одну цифру от 0 до 10, и человек должен ее угадать.
//ввод с клавиатуры. Затем во второй версии сделать счетчик, со скольки попыток человек угадал.
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Угадайте число от 0 до 20 (^_^)");
        System.out.println("Введите ваш вариант:");
        int value = scanner.nextInt(); //ввод с клавиатуры
        if(value>20){
            System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
        } else if(value<0) {
            System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
        }
        int ran = Random(0,20);
        int counter = 1;

        while(value!=ran){

            if(value!=ran){
                counter++;
            }
            System.out.println("Не угадали, попробуйте еще раз :)");
            value = scanner.nextInt();
            if(value>20){
                System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
            } else if(value<0) {
                System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
            }

        }
        System.out.println("Поздравляю, вы угадали!");
        System.out.println("Верное число было: " + ran);
        System.out.println("Колличество попыток: " + counter );


    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}
 */
//======================================================================
//V_1.2
/*
import java.util.Scanner;
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Угадайте число от 0 до 20 (^_^)");
        System.out.println("Введите ваш вариант:");
        int value = scanner.nextInt(); //ввод с клавиатуры
        int ran = Random(0,20);
        int counter = 1;

        while(value!=ran){
//            int counter = 1; //тут нельзя его ставить, так он не выйдет за пределы этого цикла, а нам нужно вернуть значение
            if(value!=ran){
                counter++;
            }
            System.out.println("Не угадали, попробуйте еще раз :)");
            value = scanner.nextInt();

        }
        System.out.println("Поздравляю, вы угадали!");
        System.out.println("Верное число было: " + ran);
        System.out.println("Колличество попыток: " + counter );


    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}

 */






//=====================================================================================================
//V_1.1
/*
import java.util.Scanner;
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Угадайте число от 0 до 2 (^_^)");
        System.out.println("Введите ваш вариант:");
        int value = scanner.nextInt(); //ввод с клавиатуры
        int ran = Random(0,2);

        while(value!=ran){
            System.out.println("Не угадали, попробуйте еще раз :)");
            value = scanner.nextInt();

        }
        System.out.println("Поздравляю, вы угадали!");
        System.out.println("Верное число было: " + ran);


    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}
 */



//======================================================================================================
//V_1(random)
/*
import java.util.Scanner;
//задача, сделать программу, которая будет загадывать рандомно одну цифру от 0 до 10, и человек должен ее угадать.
//ввод с клавиатуры. Затем во второй версии сделать счетчик, со скольки попыток человек угадал.
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Угадайте число от 0 до 10 (^_^)");
        System.out.println("Введите ваш вариант:");
        int value = scanner.nextInt(); //ввод с клавиатуры

        while(value!=Random(0,10)){
            System.out.println("Не угадали, попробуйте еще раз :)");
            value = scanner.nextInt();

        }
        System.out.println("Поздравляю, вы угадали!");
        System.out.println("Верное число было: " + value);


    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}
 */


//======================================================================================================
/*
//V_0.1
import java.util.Scanner;
public class guess_a_random_number{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);
        System.out.println("Угадайте число (^_^)");
        System.out.println("Введите ваш вариант:");
        int value = scanner.nextInt();
        while(value!=5){
            System.out.println("Не угадали, попробуйте еще раз :)");
            value = scanner.nextInt();

        }
        System.out.println("Поздравляю, вы угадали!");


    }


}

 */
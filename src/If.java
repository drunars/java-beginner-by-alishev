public class If {
    public static void main(String [] args) {
        int myInt = 15;
        if(myInt<10){
            System.out.println("да, верно");
        } else if(myInt>20){
            System.out.println("Нет, не верно");

        } else{    // будет ловить все остальные случаи, которые не подошли под предыдущие условия.
            System.out.println("He один из предыдущих случаев.");
        }

    }
}

/*  оператор if похож на цикл while, тем что тут есть условие, и если оно правидво - выполняются инструкции.
Если условие фолс, то выполняются другие инструкции.
if если условие верно в () - то выполняется инструкция в  {}
Если условие не выполняется 5<4 и знеачение folse, для этого у нас оператор else ("в противном случае, иначе")
else выполняется если if дало folse при if(5<4)


public class If { //old code :
    public static void main(String [] args) {
        if(5<4){
            System.out.println("да, верно");
        } else{
            System.out.println("Нет, не верно");

        }

    }
}

Условие проверяется сверху вниз, если допустим будет :

public class If {
    public static void main(String [] args) {
        int myInt = 5;
        if(myInt<10){
            System.out.println("да, верно");
        } else if(myInt<20){
            System.out.println("Нет, не верно");

        } else{    // будет ловить все остальные случаи, которые не подошли под предыдущие условия.
            System.out.println("He один из предыдущих случаев.");
        }

    }
}

то в таком случае выполнится первое условие if(myInt<10){ а до второго условия, не дойдет.

* */
//=====================================================================================
/*
//мой тестовый пример
public class tests{
    public static void main(String[] agrs) {
        int x = 2;
        int y = 10;
        if (x<=y){
            System.out.println( x + " меньше либо равно " + y );
        } else if(x>y){
            System.out.println(x + " больше " + y);
        } else
            System.out.println("Ни один из случаев не подошел...");
    }
}

 */

//=====================================================================================
/*
import java.util.Scanner;

public class tests{
    public static void main(String[] agrs) {
        System.out.println("Сравнение чисел");
        Scanner s = new Scanner(System.in);
        System.out.println("Введите параметр Х: ");
        int x = s.nextInt();
        System.out.println("Введите параметр Y: ");
        int y = s.nextInt();


//        int x = 2;
//        int y = 10;
        if (x<y){
            System.out.println( x + " меньше " + y );
        } else if(x>y) {
            System.out.println(x + " больше " + y);
        }else if(x==y) {
            System.out.println(x + " равен " + y);
        } else
            System.out.println("Ни один из случаев не подошел...");
    }
}

 */

//=====================================================================================
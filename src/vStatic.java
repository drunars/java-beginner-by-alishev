
public class vStatic {
    public static void main(String[] args){
        Human h1 = new Human("Bob", 40);
        Human h2 = new Human("Bob",30); //для каждого обьекта создаются свои собственные переменные.
        h1.printNumberPeople(); //выводит на экран сумму людей.
        h2.printNumberPeople();
        Human h3 = new Human("John",20);
        h3.printNumberPeople();
    }
}

class Human{
    private String name;
    private int age;

    private static int countPeople;//можно не писать =0, так как по умолчанию обьекты класса = 0.

    public Human(String name, int age){
        this.name = name;
        this.age = age;
        countPeople++; //countPeople = countPeople + 1;
    }

    public void setName(String name) {this.name = name; }

    public void setAge(int age) { this.age = age; }

    public void printNumberPeople(){
        System.out.println("Число людей: " + countPeople);
    }

}
//        System.out.println(Math.pow(2,4));
//        System.out.println(Math.PI);
//Vid18 - Сеттеры и Геттеры
public class SetGet {
    public static void main(String[] agrs){
        Person person1 = new Person();
        person1.setName("КакоетоИмя"); //назначили имя
        person1.setAge(12); //назначили возраст
        System.out.println("Выводим значение в main методе: " + person1.getName());
        System.out.println("Выводим значение в main методе: " + person1.getAge());
        person1.speak();
    }
}
class Person{
    // private String name; если делаем прайвет то это поле доступно только в пределах этого класса Person.
    private String name;
    private int age;
    //пользователь будет получать значение к полям name и age через сеттеры и геттеры.
    public  void setName(String userName) { //void ничего не возвращает, а только назначет имя пользователя этим методом.
// isEmpty() доступен у класса Стринг. Вернет ТРУ если поле заполнено, и фолс если нет
        if (userName.isEmpty()) {
            System.out.println("ТЫ ввел пустое имя");
        } else {
            name = userName;
        }
    }
    public String getName() {
        return name; //возвращается строка которая является полем в private String name
    }
    public void setAge(int userAge){
        if (userAge<0){
            System.out.println("Возраст должен быть положительным");
        } else {
            age = userAge;
        }
    }
    public int getAge(){
        return age;
    }
    int calculateYearsToRetirement(){
        int years = 65 - age;
        return years;
    }
    void speak(){
        for(int i = 0; i < 3; i++){
            System.out.println("Меня зовут " + name + ",мне " + age + " лет");
        }
    }
    void sayHello(){
        System.out.println("Hello");
    }
}
//плюс такого подхода, что мы можем менять логику внутри наших классов, и для пользователя это не отобразится.
//можем смело заменить name на login и т.д.
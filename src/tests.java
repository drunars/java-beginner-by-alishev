
public class tests {
    public static void main(String[] args) {
        int[] numbers = new int[5]; //создали массив из 5и чисел.
        // но в нем пока нет ни одного целого числа, только пустые ячейки
        numbers[0] = 10; //положили в первую ячейку 10
        System.out.println(numbers[0]);
        System.out.println(numbers[1]); //пустые ячейки у нас по умолчанию 0
        System.out.println(numbers[2]); //пустые ячейки у нас по умолчанию 0
        //создаем массив из строк
        String[] strings = new String[3]; //массив из строк,
        //и каждая из ячеек вместит в себя обьект кл. Стринг
        strings[0] = "Привет";
        strings[1] = "Пока";
        strings[2] = "Джава";
        System.out.println(strings[0] + " " + strings[2]);
        System.out.println("_______________________________");
        for (int i = 0; i<strings.length; i++ ){ //выводим все строки массива
            System.out.println(strings[i]);
        }
        System.out.println("_______________________________");
        //цикл фор ич (для каждого), сделаем что бы выводило все что есть

        for (String string:strings){
            System.out.println(string);
        }
        System.out.println("_______________________________");
        //создаем другой массив, где сразу массив с готовыми данными
        int[] numbers1 = {1,2,3};
        int sum = 0;
        for (int x:numbers1){
            sum = sum + x;
        }
        System.out.println(sum); //в конце sum будет сумма всех чисел

        //===================================================
        // Теперь о разнице примитивов и ссылочных типов данных
        int value = 0;
        String s; //не знаем со скольки она будет состоять, и сколько памяти выделять
        //и тут выделяется только ссылка, и память ток под ссылку выделяется.
        //s = null; по умолчанию пустота, пока не назначили строчку под нее
        s = "Stroka"; //вот теперь когда знаем, выделили четко под нее


        //test22
        System.out.println("Test");
        System.out.println("Test222");
        System.out.println("Test333");

    }
}






//===================================================
/*
public class tests {
    public static void main(String[] args) {
        int[] numbers = new int[5];
        //пока у нас просто создано(проинициализировано 5ь ячеек) но они пустые
 //       System.out.println(numbers[0]); //выведет 0 , так как массив пустой, и поумолчанию они 0
//        numbers[0] = 10; инициализируем массив, но если их много, лучше воспользоваться циклом for
        for(int i = 0; i<numbers.length; i++) { //numbers.length длина массива
            numbers[i] = i * 10;
        }
        //и выведем результат на экран
        for(int i = 0; i<numbers.length; i++){
            System.out.print("Номер ячейки: " + i + " | Число в ячейке: ");
            System.out.println(numbers[i]);
        }
    }
}

*/
//==============================================================================================
/*
import java.util.Scanner;
//задача, сделать программу, которая будет загадывать рандомно одну цифру от 0 до 10, и человек должен ее угадать.
//ввод с клавиатуры. Затем во второй версии сделать счетчик, со скольки попыток человек угадал.
public class tests{
    public static void main(String [] args){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Игра: 'Угадай Число'" );
        System.out.println("Введите число минимального числа, от которого нужно угадывать");
        int min = scanner.nextInt();
        System.out.println("Введите число максимального числа, от которого нужно угадывать");
        int max = scanner.nextInt();

        if(min==max){
            System.out.println("Диапазон таким не может быть! Загадывай по новой :(");
        } else if(min!=max){
            System.out.println("Угадайте число от " + min + " до " + max + " (^_^)");
            System.out.println("Введите ваш вариант:");

            int value = scanner.nextInt(); //ввод с клавиатуры
            if(value>max){
                System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
            } else if(value<min) {
                System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
            }
            int ran = Random(min,max);
            int counter = 1;

            while(value!=ran){

                if(value!=ran){
                    counter++;
                }
                System.out.println("Не угадали, попробуйте еще раз :)");
                value = scanner.nextInt();
                if(value>max){
                    System.out.println("Вы ввели число выше диапазона, конечно же вы: ");
                } else if(value<min) {
                    System.out.println("Вы ввели число ниже диапазона, конечно же вы: ");
                }

            }
            System.out.println("Поздравляю, вы угадали!");
            System.out.println("Верное число было: " + ran);
            System.out.println("Колличество попыток: " + counter );
        }




    }

    private static int Random(int min, int max) { //должен вернуть диапазон Random(0, 10)

        if (min >= max) {
            throw new IllegalArgumentException("max must be greater than min");
        }

        return (int)(Math.random() * ((max - min) + 1)) + min;
    }
}


*/
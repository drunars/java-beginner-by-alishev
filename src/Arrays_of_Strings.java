
public class Arrays_of_Strings {
    public static void main(String[] args ){
        int[] numbers = new int[5]; // создали массив с 5ью пустыми ячейками.
        //создали массив int'ов, имя массива numbers = ссылка на обьект класса целочисельного массива new int[]
        // в скобках [5] ставим размер нашего масива. Но тут нет целого числа, у нас есть конкретно 5ь пустых ячеек, который может принять на вход
        //целое число. вот ниже мы в ячейку поместим целое число 10
        numbers[0] = 10;
        System.out.println(numbers[0]);
        System.out.println(numbers[1]);
        //ниже делаем массив из строк
        String[] strings = new String[3]; //может вместить 3и обьекта класса String
        strings[0] = "Привет";
        strings[1] = "Пока";
        strings[2] = "Джава";
        //       System.out.println(strings[0] + " " + strings[2]);

        for (int i = 0; i<strings.length; i++ ){
            System.out.println(strings[i]);
        }
        System.out.println(" "); //просто пустая строка вывода
        for(String string:strings) {  //String тип данных строка, string(можно и "x" назвать) имя переменной, strings массив через который проходим
            //тут строка ссылается сначала на 1й элемент массива, затем на 2й, 3й и т.д. пока мы не пройдем весь массив.
            System.out.println(string);
        }
//создаем новый массив
        int[] numbers1 = {1,2,3}; //создали массив уже с тримя числами в ячейках
        int sum = 0;
        for(int x:numbers1){ // в этом цикле мы берем массив numbers1 и каждую иттерацию мы подставляем в эту переменную X
            // каждую из этих чисел ( в первой итерации int x = 1, во второй int x = 2,)
            sum = sum+x;

        }
        System.out.println(" ");
        System.out.println(sum);


//о строках и о массиве строк закончили, теперь про разницу между ссылочными типами данных и примитивами:

        int value = 0; //задекларировали
        String s = "stroka"; /* не можем задекларировать, так как не знаем из скольких строк, букв, будет состоять данная строка, и
        какой размер выделить под нее. Поэтому тут у нас выделяется память только под ссыдку, ссылка "s". И когда мы
        создаем переменную ссылочного типа, мы выделяем память только под ссылку. Это если РЕЧЬ ИДЕТ ПРО String s;
        А вот если мы уже создаем строку "stroka" тогда мы уже и выделяем память под данную строку. Если непишем,
        то поумолчанию String s = null; (null - это пустота), означает, что переменная s пуста.
        И только тогда когда мы уже указываем переменную s на конкретную строку, только тогда уже мы можем что то
        сделать с этой строкой "stroka".
        когда мы создали просто переменную s у нас просто создается адрес на какую то строку.
        Все равно что есть адресс дома, но дома нет. Поэтому и занимать будет места мало. А вот создали адресс, а затем дом.
        */


    }
}


/* помимо цикла for в Джава появился с какой-то версии, цикл for ich - это цикл, который проходится последовательно
по каждому елементу массива.

 */
//==================================================================================
/*
public class Arrays_of_Strings {
    public static void main(String[] args) {
        int[] numbers = new int[5]; //создали массив из 5и чисел.
        // но в нем пока нет ни одного целого числа, только пустые ячейки
        numbers[0] = 10; //положили в первую ячейку 10
        System.out.println(numbers[0]);
        System.out.println(numbers[1]); //пустые ячейки у нас по умолчанию 0
        System.out.println(numbers[2]); //пустые ячейки у нас по умолчанию 0
        //создаем массив из строк
        String[] strings = new String[3]; //массив из строк,
        //и каждая из ячеек вместит в себя обьект кл. Стринг
        strings[0] = "Привет";
        strings[1] = "Пока";
        strings[2] = "Джава";
        System.out.println(strings[0] + " " + strings[2]);
        System.out.println("_______________________________");
        for (int i = 0; i<strings.length; i++ ){ //выводим все строки массива
            System.out.println(strings[i]);
        }
        System.out.println("_______________________________");
        //цикл фор ич (для каждого), сделаем что бы выводило все что есть

        for (String string:strings){
            System.out.println(string);
        }
        System.out.println("_______________________________");
        //создаем другой массив, где сразу массив с готовыми данными
        int[] numbers1 = {1,2,3};
        int sum = 0;
        for (int x:numbers1){
            sum = sum + x;
        }
        System.out.println(sum); //в конце sum будет сумма всех чисел
       //===================================================
        // Теперь о разнице примитивов и ссылочных типов данных
        int value = 0;
        String s; //не знаем со скольки она будет состоять, и сколько памяти выделять
        //и тут выделяется только ссылка, и память ток под ссылку выделяется.
        //s = null; по умолчанию пустота, пока не назначили строчку под нее
        s = "Stroka"; //вот теперь когда знаем, выделили четко под нее

    }
}
*/
//===============================================================
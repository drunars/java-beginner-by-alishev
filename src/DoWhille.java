//пишем программу которая будет считывать ввод цифр с клавиатуры, пока не считает 5
//version 2
import java.util.Scanner; // Alt Enter

public class DoWhille {
    public static  void  main(String [] agrs) {
        Scanner scanner = new Scanner(System.in); // создали переменную класса Сканер (Scanner scanner) и ссылаем на обьект класса сканер (new Scanner() );
        int value; //декларируем переменную тут, что бы она была видна в пределах этих фигурных скобок main, ибо если в самом Do то value не увидит int
        do{                  //сначала выполяет инструкции,а затем проверяет условие.
            System.out.println("Введи 5");
            value = scanner.nextInt();
        } while(value!=5);

//        while(){ вайл сначала проверяет условие, а затем выполняет инструкции.
        System.out.println("Вы ввели " + value);
    }

}

/*
version_1 (в версии 2 убираем дублирывание кода, ибо в хорошей программе такого быть не должно)
цикл DoWhile в помощь =)
public class DoWhille {
    public static  void  main(String [] agrs) {
        Scanner scanner = new Scanner(System.in); // создали переменную класса Сканер (Scanner scanner) и ссылаем на обьект класса сканер (new Scanner() );
        System.out.println("Введите 5 ");
        // параметр (System.in) создаем входной поток
        int value = scanner.nextInt(); // в переменную велью вместили то что мы передаем с клавиатуры
        while(value!=5) {               // (!=5 не равно 5и)счетчик будет крутится пока велью не будет равен 5
            System.out.println("Введите еще раз 5");
            value = scanner.nextInt();
        }
        System.out.println("Вы ввели " + value);
    }

}

 */
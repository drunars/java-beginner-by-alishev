//операторы которые работают с циклами - Break , Continue
//Break используется внутри цикла, он обрывает цикл

public class Break_Continue {
    public static void main(String [] args) {
        for(int i = 0; i<=15; i++) {
            if(i%2==0){ // % значит поделить i на 2 и посмотреть остаток - "остаток от деления"
                continue; //исходя из условия, континуе выполняется только когда i будет четным числом
            }
            System.out.println("Это нечетное число " +i);
        }


    }
}


/* OPERATOR BREAK:
public class Break_Continue {
    public static void main(String [] args) {
        int i = 0;
        while(true) {
            if(i==15){ //без if будет бесконечный счет, добавили if break, что бы выйти из цикла, после i==15 - true выход из цикла
                break;
            }
            System.out.println(i);
            i++;
        }
        System.out.println("Мы вышли зи цикла.");

    }
}


 */
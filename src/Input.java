
//java.lang //пакет джава ленг (для стринг) портируются уже заранее
//java.util //нужно импортировать самим вручную
import java.util.Scanner; //IDE предложила вручную самостоятельно импортировать при нажатии в коде на Сканер

public class Input {
    public static  void main(String [] args) {
//        String s = "stroka"; //String class - "stroka" object класса String
//        String s = new String("stroka"); //можно и просто String s = "stroka";
        //class Scaner отвечает за ввод данных с консоли от пользователя
//        String string = new String("Stroka"); //Class String заранее импортирован а класс Scanner нет
        Scanner s = new Scanner(System.in); //в параметры Сканер мы должны задать входной поток System.in стандартный поток данных
        //Scanner это класс, а с помощью new Scanner мы задаем новый обьект класса сканер, и теперь s ссылается на обьект класса Сканер
        System.out.println("Введите что-нибудь");
  //      String string = s.nextLine(); // метод nextLine считывает с клавиатуры одну строчку вводных данных
        //и собственно в этой переменной string будет хранится то, что мы ввели с клавиатуры.
  //      System.out.println("Вы ввели: " + string);
        int x = s.nextInt(); //ввод цифр
        System.out.println("Вы ввели: " + x);
    }
}


//=====================================================================================

/*
//мой пример Input с else if

import java.util.Scanner;

public class tests{
    public static void main(String[] agrs) {
        System.out.println("Сравнение чисел");
        Scanner s = new Scanner(System.in);
        System.out.println("Введите параметр Х: ");
        int x = s.nextInt();
        System.out.println("Введите параметр Y: ");
        int y = s.nextInt();


//        int x = 2;
//        int y = 10;
        if (x<y){
            System.out.println( x + " меньше " + y );
        } else if(x>y) {
            System.out.println(x + " больше " + y);
        }else if(x==y) {
            System.out.println(x + " равен " + y);
        } else
            System.out.println("Ни один из случаев не подошел...");
    }
}




 */
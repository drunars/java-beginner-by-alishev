
public class ClassesAndObjects { //паблик класс это главный класс в джава файле
    //имя джава"файла" и публичного класса должны совпадать.
    //класс это шаблон, общее понятие.
    public static void main(String[] agrs){
        Person person1 = new Person(); //person1(переменная) обьект класса типа Person
 //       person1.name = "Roma";
        person1.setNameAndAge("Roma",50);
 //       person1.age = 50;
        String s1 = "Vova";
        int s2 = 20;
        Person person2 = new Person();
        person2.setNameAndAge(s1,s2);
 //       person2.name = "Vova";
  //      person2.age = 20;
        //вызываем метод спик person1.speak(); метод мы пишем с параметрами();
 //       person1.calculateYearsToRetirement();
 //       person2.calculateYearsToRetirement();
 //       int year1 = person1.calculateYearsToRetirement();
 //       System.out.println("Роме до пенсии: " + year1 + " лет");
        person1.speak();
        person2.speak();

    }
}
// Может быть любое количество классов //методы мы всегда пишем с маленькой буквы
class Person{
    //У класса могут быть:
    //1. Данные (поля) (наприме: возраст человека, рост, вес ...)
    //2. Действия, которые он модет совершать (методы)
    String name;
    int age;
    //создаем метод void, воид "пустота" - тип возвращаемого обьекта. Войд ничего не возвращает, ни каких типов данных.
    void speak(){ //Теперь у каждого класса person имеется метод speak - который выводит на экран имя и возраст.
        for(int i = 0; i < 3; i++){
            System.out.println("Меня зовут " + name + ",мне " + age + " лет");
        }
 //       System.out.println("Меня зовут " + name + ",мне " + age + " лет");
    }
    void sayHello(){
        System.out.println("Hello");
    }
    int calculateYearsToRetirement(){ //void calculateYearsToRetirement(){
        int years = 65 - age;
        return years; //при int вместо void, метод возвращает интовое значение, сколько лет до пенсии , расчет.
//        System.out.println("Колличество лет до пенсии: " + years); //при воид метод выдавал текст на экран,
//        но ничего не овзвращал
    }

//return по мимо возвращения, программа когда натыкаетя на ретюрн - она сразу выходит из метода. После ретурн
// нет смысла ничего писать, все равно не выолнит.
//если указан один тип возвращаемых значений, то мы не можем вернуть другой. Если указан инт, то стринг не
// вернем, будет ошибка естественно.
//данные из метода, вызываются сразу в место их вызова.
//Например так же может возвращать массивы.
//Урок 17 (Пераметр метода)
//
 //   void  setName(String username) {      //эквивалентен этой строчке person1.name = "Roma";
        // метод setName принимает Строку username
 //       name = username; // у каждого обьекта поле name = тому значение что поступило в параметры,
//       и воспринимается тут как username
        //username это только в пределах тела этого метогда. Но поставлять выше переменные мы можем
// любыми переменными, например s1. Главное вернуть Стринг
        // параметров может быть сколько угодно, например имя, возраст и тд
 //   }
    void setNameAndAge(String username, int userAge){
        name = username;
        age = userAge;
    }


}

 
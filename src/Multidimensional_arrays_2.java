//отом как выводить двух мерный массив на экран ( продолжение //Многомерные массивы Multidimensional_arrays )

public class Multidimensional_arrays_2 {
    public static void main(String[] args) {
        int[] number = {1,2,3};

        int[][] matrice = {{1,2,3},  //внешний цикл будет проходится по строкам
                           {4,5,6},  //а внутренний цикл будет проходится по столбцам
                           {7,8,9}};

        for(int i = 0; i<matrice.length; i++){  //внешний цикл, и matrice.lenght имеет значение 3, потому как у нас 3и одномерных массива
            for(int j = 0; j<matrice[i].length; j++){  //а каждый раз когда мы находимся на одной из строк,  внутренний цикл проходится по каждому элементы в этой строке
                //и сдесь мы обращаемся к каждой из его строк [i] (matrice[i]) и мы запрашиваем его длину.
                System.out.print(matrice[i][j]+" ");
            }
            System.out.println();
        }
    }
}
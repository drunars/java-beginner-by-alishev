/*
Switch во многом похож на оператор if но используется во многих условиях где нужно много перебрать,
и что бы не писать кучу if'ов удобнее будет воспользоваться Switch.
 */

import java.util.Scanner;

public class Switch {
    public static  void main(String [] args ) {
        Scanner scanner = new Scanner(System.in); //System.in - входной поток.
        System.out.println("Введите возраст");
        int age = scanner.nextInt(); // будем хранить то, что введем с клавиатуры (число) nextInt принимает только числа
//      String age = scanner.nextLine(); пример перебора Стринговой переменной свичем
        switch(age){   //поместили переменную, которая принимает различные значения. (с 7ой джавы можем и String перебирать)
//          case "ноль" : // пример перебора строк
            case 0 :   // в кейсах перебираем все эти различные значения
                System.out.println("Ты родился");
                break; //оператор брейк прерывает выполнение цикла и блока свич
            case 7 :
                System.out.println("Ты пошел в школу");
                break;
            case 18 :
                System.out.println("Ты закончил школу");
                break;
            default: // если не одно из условий не подходит
                System.out.println("Не одно из предыдущих условий не подошло");
        }


    }
}

/*
Пример с if - в данном случае не удобно, так как 100 вариаций к примеру if'ами вписывать смысла нет, очень геморно.
Так что лучше использовать в таких моментах Switch, что бы переберать множества условий.
import java.util.Scanner;

public class Switch {
    public static  void main(String [] args ) {
        Scanner scanner = new Scanner(System.in); //System.in - входной поток.
        System.out.println("Введите возраст");
        int age = scanner.nextInt(); // будем хранить то, что введем с клавиатуры (число) nextInt принимает только числа
        if(age==10){
            System.out.println("Ты учишься в школе");
        }else if(age==18){
            System.out.println("Ты закончил школу");
        }else if()


    }
}

 */
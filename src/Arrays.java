public class Arrays {
    public static void main(String[] args) {
//1й способ подходит, когда нужно создать пустой массив, и уже затем вносить в него данные
        int number = 10; //примитивный тип данных //[10]
        int[] numbers = new int[5]; // все элементе в этом массиве по умолчанию равны 0, потому что мы их не инициализировали
//        System.out.println(numbers[0]); // расчет идет с 0, поэтому тут элементы в массиве 0 1 2 3 4 (а вот пятерки следовательно нет)
//        numbers[0] = 10; можно и так делать, но если массив из 1000+ то лучше делать при помощи цикла FOR, он оч удобно подходит для инициализации массивов.
//        numbers[1] = 20;
        for(int i = 0; i<numbers.length; i++){   /*i начинает от= 0; i++ инкрементируется до тех пор
        пока i меньше длины массива.Что бы узнать длину массивы, мы пишем имя массива numbers.length */
            numbers[i] = i*10;

        }
        for(int i = 0; i<numbers.length; i++){
            System.out.println(numbers[i]);
        }
//2й способ, если мы уже заранее знаем числа, которые нам нужно поместить в массив
        //если мы заранее знаем, какие числа у нас должны быть в массиве, мы инициализируем как в примере ниже:
        int[] numbers1 = {1,2,3};
        for(int i = 0; i<numbers1.length; i++ ){
            System.out.println(numbers1[i]);
        }

    }
}
//======================================================================================
/*
public class Arrays {
    public static void main(String[] args) {
        int[] numbers = new int[5];
        //пока у нас просто создано(проинициализировано 5ь ячеек) но они пустые
        //       System.out.println(numbers[0]); //выведет 0 , так как массив пустой, и поумолчанию они 0
//        numbers[0] = 10; инициализируем массив, но если их много, лучше воспользоваться циклом for
        for(int i = 0; i<numbers.length; i++) { //numbers.length длина массива
            numbers[i] = i * 10;
        }
        //и выведем результат на экран
        for(int i = 0; i<numbers.length; i++){
            System.out.print("Номер ячейки: " + i + " | Число в ячейке: ");
            System.out.println(numbers[i]);
        }
        //Ниже пример, если мы заранее знаем, что мы хотим поместить в массив:
        int[] numbers1 = {1,2,3};
//        System.out.println(numbers1[0]);
        for(int i = 0; i<numbers1.length; i++){
            System.out.println(numbers1[i]);
        }
    }
}



 */
//========================================================================================
/*


public class Arrays {
    public static void main(String[] args) {
        //  есть приметивные типы данных, а есть ссылочные
//        int number = 10; //коробка [10]примитивынй тип данных, такие как булеан, чар и тд (данные переменных лежат прямо в коробке)
//        char character = 'a'; // примитивынй тип данных
//        String s = "Hello"; //Ссылочный тип данных. Kласс String, s имя переменной, "Hello" обьект класса стринг
//        String s1 = new String("Hello"); // вверху упрощенный вариант, а тут как и с другими Классами создание.
        //в ссылочкных типах данных, в коробке лежит ссылка на обьект, а не сам обьект ( как в примитивных)
        int number = 10; //в этой коробке лежит [10]
        int[] numbers = new int[5]; /* 5 размер массива.Переменную numbers ссылаем на новый обьект класса массив,
        в масивве - numbers ссылается на -> [массив], потому и наз. ссылочным типом данных.
        Теперь ячейки нужно проинициализировать. В джаве кстати, по умолчанию ячейки инициализируются как 0
           System.out.println(numbers[0]);

          }
}


*/




/*

public class tests {
    public static void main(String[] args) {
        int number = 10;
        int[] numbers = new int[5];
        for(int i = 0; i<numbers.length; i++){
            numbers[i] = i*10;
        }
        for(int i = 0; i<numbers.length; i++){
            System.out.println(numbers[i]);
        }
        int[] numbers1 = {1,2,3};
        for(int i = 0; i<numbers1.length; i++ ){
            System.out.println(numbers1[i]);
        }

    }
}


*/